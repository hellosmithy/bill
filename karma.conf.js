/* eslint-disable */
var webpackConfig = require('./webpack.config');

module.exports = function(config) {
	config.set({
		browsers: [ 'Chrome' ],
		files: [
			'test/tests.bundle.js'
		],
		frameworks: [ 'chai', 'mocha' ],
		plugins: [
			'karma-chai',
			'karma-chrome-launcher',
			'karma-mocha',
			'karma-mocha-reporter',
			'karma-osx-reporter',
			'karma-sourcemap-loader',
			'karma-webpack',
		],

		osxReporter: {
			notificationMode: 'change'
		},

		// run the bundle through the webpack and sourcemap plugins
		preprocessors: {
			'test/tests.bundle.js': [ 'webpack', 'sourcemap' ]
		},
		reporters: [ 'mocha', 'osx' ],
		singleRun: false,
		webpack: webpackConfig,
		webpackMiddleware: {
			noInfo: true
		}
	});
};
