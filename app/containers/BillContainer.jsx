'use strict';

import React from 'react/addons';
import connectToStores from 'alt/utils/connectToStores';

import BillStore from 'flux/stores/BillStore';
import BillActions from 'flux/actions/BillActionCreator';
import Bill from 'components/Bill/Bill.jsx';

@connectToStores
export default class BillContainer extends React.Component {

	static getStores() {
		return [BillStore]
	}

	static getPropsFromStores() {
		return BillStore.getState()
	}

	componentDidMount() {
		BillStore.fetch();
	}

	render() {
		return (
			<Bill { ...this.props } />
		);
	}

}
