import alt from 'flux/alt.js';
import BillActions from 'flux/actions/BillActionCreator';
import BillSource from 'flux/datasources/BillSource';

class BillStore {

	state = {
		bill: null,
		error: null,
		loading: null
	}

	constructor() {
		this.bindListeners({
			handleGetBillSuccess: BillActions.GET_BILL_SUCCESS,
			handleGetBillFailure: BillActions.GET_BILL_FAILURE,
			handleGetBillLoading: BillActions.GET_BILL_LOADING
		});
		this.registerAsync(BillSource);
		this.exportPublicMethods({
			fetch: this.fetch
		});
	}

	fetch = () => {
		if (!this.getInstance().isLoading()) {
			this.getInstance().getBill();
		}
	}

	handleGetBillSuccess(bill) {
		this.setState({ bill, loading: false });
	}

	handleGetBillFailure(error) {
		this.setState({ error, loading: false });
	}

	handleGetBillLoading() {
		this.setState({ loading: true });
	}

}

export default alt.createStore(BillStore, 'BillStore');
