import BillActions from 'flux/actions/BillActionCreator.js';

const ENDPOINT = 'https://still-scrubland-9880.herokuapp.com/bill.json';

const BillSource = {
	getBill: {

		remote(state) {
			return fetch(ENDPOINT)
				.then(response => response.json());
		},

		loading: BillActions.getBillLoading,
		success: BillActions.getBillSuccess,
		error: BillActions.getBillFailure,

	}
};

export default BillSource;
