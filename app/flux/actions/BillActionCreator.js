import alt from 'flux/alt.js';

const BillActions = alt.generateActions(
	'getBillLoading', 'getBillSuccess', 'getBillFailure'
);

export default BillActions;
