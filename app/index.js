'use strict';

import 'babel-core/polyfill';
import React from 'react/addons';
import BillContainer from 'containers/BillContainer.jsx';
import './styles.css';

React.render(<BillContainer />, document.body);
