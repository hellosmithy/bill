'use strict';

import React from 'react/addons';
import styles from './SubscriptionsCharges.css';

import currency from 'utils/currency';

export default class SubscriptionsCharges extends React.Component {

	static propTypes = {
		subscriptions: React.PropTypes.array,
		total: React.PropTypes.number
	}

	render() {
		return (
			<div className={ styles.SubscriptionsCharges }>
				<ul className={ styles.list }>
					{ this.props.subscriptions.map((subscription, index) => {
						return (
							<li key={ index }>
								<span>{ subscription.type }</span>
								<span>{ subscription.name }</span>
								<span>{ currency.format(subscription.cost) }</span>
							</li>
						);
					}) }
				</ul>

				<h4>SUBTOTAL: { currency.format(this.props.total) }</h4>
			</div>
		);
	}

}
