/*eslint expr: true*/

import { default as chai, expect } from 'chai';

import React from 'react/addons';
import SubscriptionsCharges from './SubscriptionsCharges.jsx';

const TestUtils = React.addons.TestUtils;

describe('SubscriptionsCharges', () => {

	let component;
	let mockSubscriptions = [
		{ "type": "tv", "name": "Variety with Movies HD", "cost": 50.00 },
		{ "type": "talk", "name": "Sky Talk Anytime", "cost": 5.00 },
		{ "type": "broadband", "name": "Fibre Unlimited", "cost": 16.40 }
	];

	beforeEach(() => {
		component = TestUtils.renderIntoDocument(
			<SubscriptionsCharges subscriptions={ mockSubscriptions } total={ 71.40 } />
		);
	});

	it('should render a h4 with formatted total', () => {
		let h4 = TestUtils.findRenderedDOMComponentWithTag(
			component, 'h4'
		);
		expect(h4.getDOMNode().textContent)
			.to.equal('SUBTOTAL: £71.40');
	});

	it('should render a list of subscriptions', () => {
		let ul = TestUtils.findRenderedDOMComponentWithTag(
			component, 'ul'
		);
		expect(ul.getDOMNode().children)
			.to.have.length(3);
	});

});
