'use strict';

import React from 'react/addons';
import styles from './SkyStoreCharges.css';

import currency from 'utils/currency';

export default class SkyStoreCharges extends React.Component {

	static propTypes = {
		rentals: React.PropTypes.array,
		buyAndKeep: React.PropTypes.array,
		total: React.PropTypes.number
	}

	render() {
		return (
			<div className={ styles.SkyStoreCharges }>

				<h5>Rentals</h5>
				<ul className={ styles.list }>
					{ this.props.rentals.map((item, index) => {
						return (
							<li key={ index }>
								<span>{ item.title }</span>
								<span>{ currency.format(item.cost) }</span>
							</li>
						);
					}) }
				</ul>

				<h5>Buy and Keep</h5>
				<ul className={ styles.list }>
					{ this.props.buyAndKeep.map((item, index) => {
						return (
							<li key={ index }>
								<span>{ item.title }</span>
								<span>{ currency.format(item.cost) }</span>
							</li>
						);
					}) }
				</ul>

				<h4>SUBTOTAL: { currency.format(this.props.total) }</h4>
			</div>
		);
	}

}
