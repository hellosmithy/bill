/*eslint expr: true*/

import { default as chai, expect } from 'chai';

import React from 'react/addons';
import SkyStoreCharges from './SkyStoreCharges.jsx';

const TestUtils = React.addons.TestUtils;

describe('SkyStoreCharges', () => {

	let component;
	let rentalsMock = [
		{ "title": "The Incredibles", "cost": 4.99 },
		{ "title": "Fargo", "cost": 4.99 },
	];
	let buyAndKeepMock = [
		{ "title": "Free Willy", "cost": 2.99 }
	];

	beforeEach(() => {
		component = TestUtils.renderIntoDocument(
			<SkyStoreCharges
				rentals={ rentalsMock }
				buyAndKeep={ buyAndKeepMock }
				total={ 8.52 } />
		);
	});

	it('should render a h4 with formatted total', () => {
		let h4 = TestUtils.findRenderedDOMComponentWithTag(
			component, 'h4'
		);
		expect(h4.getDOMNode().textContent)
			.to.equal('SUBTOTAL: £8.52');
	});

});
