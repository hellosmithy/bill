/*eslint expr: true*/

import { default as chai, expect } from 'chai';

import React from 'react/addons';
import Bill from './Bill.jsx';
import mockBill from 'data/mockBill';

const TestUtils = React.addons.TestUtils;

describe('Bill', () => {

	let component;

	beforeEach(() => {
		component = TestUtils.renderIntoDocument(
			<Bill bill={ mockBill } loading={ false } />
		);
	});

	it('should render a h1', () => {
		let h1 = TestUtils.findRenderedDOMComponentWithTag(
			component, 'h1'
		);
		expect(h1.getDOMNode().textContent)
			.to.equal('Your Bill');
	});


});
