'use strict';

import React from 'react/addons';
import styles from './Bill.css';

import Statement from 'components/Statement/Statement.jsx';
import SubscriptionsCharges from 'components/SubscriptionsCharges/SubscriptionsCharges.jsx';
import CallCharges from 'components/CallCharges/CallCharges.jsx';
import SkyStoreCharges from 'components/SkyStoreCharges/SkyStoreCharges.jsx';

import moment from 'moment';
import currency from 'utils/currency';

export default class Bill extends React.Component {

	static propTypes = {
		bill: React.PropTypes.object,
		loading: React.PropTypes.bool
	}

	render() {
		return (
			<div className={ styles.Bill }>
				{
					this.props.bill
						? this.renderBill()
						: this.renderLoader()
				}
			</div>
		);
	}

	renderLoader = () => {
		return <h1>Loading...</h1>
	}

	renderBill = () => {
		return (
			<bill-element>
				<h1>Your Bill</h1>
				<Statement
					generated={ this.props.bill.statement.generated }
					due={ this.props.bill.statement.due }
					periodFrom={ this.props.bill.statement.period.from }
					periodTo={ this.props.bill.statement.period.to } />

				<h3>TOTAL: { currency.format(this.props.bill.total) }</h3>
				<hr />

				<h4>Cost Breakdown:</h4>
				<h4>Subscriptions</h4>
				<SubscriptionsCharges
					subscriptions={ this.props.bill.package.subscriptions }
					total={ this.props.bill.package.total } />

				<hr />

				<h4>Call Charges</h4>
				<CallCharges
					charges={ this.props.bill.callCharges.calls }
					total={ this.props.bill.callCharges.total } />

				<hr />

				<h4>SkyStore Charges</h4>
				<SkyStoreCharges
					rentals={ this.props.bill.skyStore.rentals }
					buyAndKeep={ this.props.bill.skyStore.buyAndKeep }
					total={ this.props.bill.skyStore.total } />
			</bill-element>
		);
	}

}
