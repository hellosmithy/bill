'use strict';

import React from 'react/addons';
import styles from './CallCharges.css';

import currency from 'utils/currency';

export default class CallCharges extends React.Component {

	static propTypes = {
		charges: React.PropTypes.array,
		total: React.PropTypes.number
	}

	render() {
		return (
			<div className={ styles.CallCharges }>
				<ul className={ styles.list }>
					{ this.props.charges.map((charge, index) => {
						return (
							<li key={ index }>
								<span>{ charge.called }</span>
								<span>{ charge.duration }</span>
								<span>{ currency.format(charge.cost) }</span>
							</li>
						);
					}) }
				</ul>
				<h4>SUBTOTAL: { currency.format(this.props.total) }</h4>
			</div>
		);
	}

}
