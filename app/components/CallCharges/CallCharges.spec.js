/*eslint expr: true*/

import { default as chai, expect } from 'chai';

import React from 'react/addons';
import CallCharges from './CallCharges.jsx';

const TestUtils = React.addons.TestUtils;

describe('CallCharges', () => {

	let component;
	let mockCharges = [
		{ "called": "07716393769", "duration": "00:23:03", "cost": 2.13 },
		{ "called": "07716393769", "duration": "00:23:03", "cost": 2.13 },
		{ "called": "07716393769", "duration": "00:23:03", "cost": 2.13 },
		{ "called": "02074351359", "duration": "00:23:03", "cost": 2.13 }
	];

	beforeEach(() => {
		component = TestUtils.renderIntoDocument(
			<CallCharges charges={ mockCharges } total={ 8.52 } />
		);
	});

	it('should render a h4 with formatted total', () => {
		let h4 = TestUtils.findRenderedDOMComponentWithTag(
			component, 'h4'
		);
		expect(h4.getDOMNode().textContent)
			.to.equal('SUBTOTAL: £8.52');
	});

	it('should render a list of call charges', () => {
		let ul = TestUtils.findRenderedDOMComponentWithTag(
			component, 'ul'
		);
		expect(ul.getDOMNode().children)
			.to.have.length(4);
	});

});
