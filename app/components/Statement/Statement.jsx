'use strict';

import React from 'react/addons';
import styles from './Statement.css';

import moment from 'moment';

export default class Statement extends React.Component {

	static propTypes = {
		generated: React.PropTypes.string,
		due: React.PropTypes.string,
		periodFrom: React.PropTypes.string,
		periodTo: React.PropTypes.string
	}

	render() {
		return (
			<div className={ styles.Statement }>
				<p>{ moment(this.props.generated).format('LL') }</p>
				<hr />
				<p>Due on: <strong>{ moment(this.props.due).format('LL') }</strong></p>
				<p>Period: <strong>{ moment(this.props.periodFrom).format('LL') } – { moment(this.props.periodTo).format('LL') }</strong></p>
			</div>
		);
	}

}
