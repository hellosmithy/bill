export default {
	format: (value) => {
		return value.toLocaleString('en-GB', {
			style: 'currency',
			currency: 'GBP',
			minimumFractionDigits: 2
		});
	}
}
