# Bill

#### Getting started:

```
npm start
```

Browse to [http://localhost:8080/webpack-dev-server/](http://localhost:8080/webpack-dev-server/)


#### Running the tests:

```
npm run test
```
