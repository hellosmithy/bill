/*eslint-disable */

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var node_modules = path.resolve(__dirname, 'node_modules');

module.exports = {
	context: path.join(__dirname, './app'),
	entry: {
		app: [
			path.resolve(__dirname, 'app/index.js'),
			'webpack/hot/dev-server'
		]
	},
	devtool: 'inline-source-map',
	module: {
		loaders: [
			{
				test: /\.spec\.js|\.jsx?$/,
				loaders: ['react-hot-loader','babel-loader?stage=0'],
				include: path.join(__dirname, 'app')
			},
			{ test: /\.css$/,
				loaders: [
					'style-loader?singleton',
					'css-loader?modules&localIdentName=[path]--[local]',
					'postcss-loader'
				],
				include: path.join(__dirname, 'app')
			}
		],
		noParse: [
			path.resolve(node_modules, 'react/dist/react.min.js'),
			path.resolve(node_modules, 'sinon/lib/sinon.js')
		]
	},
	postcss: [
		require('postcss-normalize')
	],
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Bill',
			template: 'app/index.html',
			inject: true
		})
	],
	resolve: {
		modulesDirectories: ['node_modules', 'app']
	}
};
